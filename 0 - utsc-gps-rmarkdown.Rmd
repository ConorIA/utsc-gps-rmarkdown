---
title: 'Literate Programming Made Easy with R Markdown'
author: "Conor I Anderson"
date: "18/07/2020"
bibliography: "bibliography.bib"
link-citations: true
params:
  bash: false
output: 
  html_document:
    toc: yes
    toc_float: yes
---

# Before we begin

Before we begin, we should check to ensure that we have everything we need to run these books. The *setup.R* script will help.

# What is Markdown

Markdown is a markup language created by in 2004 by John Gruber with Aaron Swartz for the [Daring Fireball](https://daringfireball.net/) blog.

Markdown was originally written to turn a simple, easy-to-remember (and easy-to-read) markup into valid HTML. You can play around with Markdown using the [Markdown Web Dingus](https://daringfireball.net/projects/markdown/dingus).

Markdown syntax is stored in a separate document because it messes up the Table of Contents!

```{r, child="1 - markdown syntax.md", eval=FALSE}

```

## Exercise 1

Take the interactive [Markdown Tutorial](https://www.markdowntutorial.com/) for a spin.

# What is R Markdown?

R Markdown is an extension of Markdown for **literate programming**. Literate programming was created by Donald Knuth (the creator of TeX). Literate programming is the practice in which a computer program, in this case, R Markdown and ultimately the Pandoc program, is provided instructions in natural language interspersed with snippets of code. Ultimately, two outputs are created, one with the source code, and another version of human-facing documentation.

R Markdown builds on the simplicity of Markdown, leveraging the power of Pandoc, to allow literate programming in Markdown!

## The anatomy of an R Markdown document

An R Markdown document is composed of:

-   A YAML Header that defines options → **Let's go see what that looks like**
-   Code chunks or inline code that are run by *engines* (more on engines later)
-   Regular Markdown

# The YAML Header

```{r, child="2 - yaml header.Rmd"}

```

# R Markdown code and code chunks

You can write a line like this that will run code inline.

This line was executed at `r Sys.time()`.

However, you will probably primarily be using code chunks.

## Code chunks

You can insert a code chunk from the button at the top right, or by typing Ctrl-Alt-I

```{r}
"This is a code chunk"
```

The chunk has a specific syntax defined in curly braces.

```         
If you don't use curly braces, you get a plain Markdown code block
This code isn't executed. It is just whatever you typed here. 
```

Let's look at the code syntax.

```{r}
cat("The first item in the code chunk is the engine. This specifies which program will be used to run this code. In this case, we are using R (though the r is lowercase in the code chunk)")
```

```{r labelled_chunk}
cat("An optional second element is the chunk label. Chunk labels are very useful for keeping track as the document compiles. Chunk labels must be unique.")
```

```{r chunk_options, message=FALSE}
cat("Chunk options allow you to tweak how the code in a chunk is run and/or rendered.")
message("For instance, this message won't be printed because message is FALSE")
```

### Chunk options controlling how code will be run and displayed

-   `echo`: Should the R code be printed? FALSE will show only the output, but not the input
-   `eval`: Should the R code by run? FALSE is useful for including examples, but not running. This is handy if you want to show how to install a package, but you already have it installed!
-   `include`: Should the code (and output) appear in the document at all? FALSE *will* execute the code, but it won't appear in the final document. Useful if you want to set some options in the background or download some data to display.
-   `cache`: if cache is TRUE, then results will be re-used when code is not changed; useful if you have a long-running function or some function that downloads data from an external source, for instance.

```{r, echo=FALSE}
x <- 10
cat("In this chunk, we set the value of x to", x)
cat("\nYou don't see the code though because echo=FALSE")
```

```{r, eval=FALSE}
x <- 30
cat("This chunk would set the value of x to", x, "but it won't be evaluated.")
```

```{r}
cat("The value of x is still", x)
```

```{r, include=FALSE}
x <- 45
```

```{r}
cat("We changed the value of x to", x, "in a chunk with include=FALSE")
```

### Chunk options for figures

You can control features of figures in the chunk options

```{r}
plot(cars, main = "Figure with defaults")
```

```{r, fig.width=10}
plot(cars, main = "Figure w=10")
```

```{r, fig.height=10, fig.width=10}
plot(cars, main = "Figure w=10 h=10")
```

```{r, fig.cap="This is a caption!"}
plot(cars, main = "Figure w/ caption")
```

### Chunk options for code appearance

```{r, highlight=FALSE}
print("Syntax highlighting has been turned off for this code")
```

```{r, prompt=TRUE}
print("Code is prefaced with a prompt")
```

### Setting chunk options globally

You can set chunk options for all following code (typically this would be the first code chunk in your document)

```{r global_opts}
knitr::opts_chunk$set(echo = TRUE,
                      fig.cap = "This is a pre-defined caption")
```

```{r pressure}
plot(pressure)
```

```{r pressure2, fig.cap="YAML instructions can be overridden at the chunk level"}
plot(pressure)
```

## Exercise 3

Take a moment to read the "Chunk Options" section of [R Markdown Reference](https://rstudio.com/wp-content/uploads/2015/03/rmarkdown-reference.pdf) and experiment with some of the chunk options.

# Engines

R is not the only programming language that can be used for R Markdown. You can query the engines that are available.

```{r test_name}
names(knitr::knit_engines$get())
```

## Python

Python is implemented by the reticulate package and the functionality is particularly rich and interoperable with R thanks to the reticulate package.

```{r}
library(reticulate)
```

```{python}
print("You can run Python code")
```

```{python}
print("Use the r object in Python to access objects from R")
R = r['x']
print("Remember, we had set x to {}.".format(R))
```

This works in reverse too.

```{python}
y = 99.87
```

```{r}
cat("We used Python to define y as", py$y)
```

## Bash

If you are on Mac, Linux, or have WSL installed, you can run commands directly on your system through Bash

```{bash, eval=params$bash}
echo $USER
```

This is a little neater than running through the `system()` function.

```{r, eval=params$bash}
system("echo $USER")
```

There does not seem to be any built-in interoperability between Bash and R, but you could use a temp file to exchange results.

```{bash, eval=params$bash}
echo "We will save the result of 3 to a text file"
echo 3 > textfile
```

```{r, eval=params$bash}
x <- as.integer(readLines("textfile"))
cat("x is", x)
unlink("textfile")
```

You don't actually need any R code in an R Markdown file, other than the setup options!

## Exercise 4

Use the language of your choice to create a value. Then exchange that value with a different language.

# Interfacing between code and markup

You can ask knitr to output results "asis", and they will be interpreted literally by Pandoc.

```{r}
# Not asis
cat("**This text is bold**")
```

```{r, results='asis'}
# Yes asis
cat("**This text is bold**")
```

You can use the `kable()` function to pretty-print tables.

```{r}
head(iris)
```

```{r}
knitr::kable(head(iris), "markdown")
```

Or packages to insert icons into your document.

```{r}
# Not asis
cat(fontawesome::fa("r-project", "100", "blue"))
```

```{r, results="asis"}
# Yes asis
cat(fontawesome::fa("r-project", "100", "blue"))
```

Bear in mind that you risk introducing non-convertible markup by doing this. e.g. pure HTML code might now look right in LaTeX.

# Defining custom hooks

You can also define custom hooks to run code before or after your code chunks. Let's combine some of the skills we've just seen.

```{r, child="3 - hooks.Rmd"}

```

# Other formats / packages

There are many packages that extend R Markdown. Let's look a a few of these.

-   slides (ioslides, slidy, beamer)
-   **xaringan** Ninja Presentation
-   **rticles** scholarly article
-   **learnr** tutorial
-   **bookdown** to create entire books - [example](https://claut.gitlab.io/man_ccia)
-   **blogdown** to create websites - [example](https://conr.ca/)
-   Generic templates can also be created. e.g. Github [svmiller/svm-r-markdown-templates](https://github.com/svmiller/svm-r-markdown-templates)

# Capstone Exercise

-   For GPS credit
-   Create a test or quiz using **learnr**.
-   The topic can be of your choice (R Markdown, or something relevant to you).
-   Include a minimum of 10 questions.
-   Send the document via email to [conor.anderson\@utoronto.ca](mailto:conor.anderson@utoronto.ca){.email}

------------------------------------------------------------------------

# References
