---
title: "Hooks"
author: "Conor I Anderson"
date: "19/07/2020"
params:
  bash: false
output: html_document
---

## Load packages we'll need

```{r hook_setup}
# Knitr chunk options
knitr::opts_chunk$set(echo = TRUE)

# To load Python
library(reticulate)
use_miniconda("rmarkdown")

# Used for the second hook
library(fontawesome)

# Used in the examples
library(httr)
```

```{python py_setup}
# Python libraries that we will use
import requests
import pandas as pd
from io import BytesIO
```

## Define our first hook

```{r first_hook}
knitr::knit_hooks$set(first = function(before, options) {
  if (before) {
    "This content appears before the code block."
  } else {
    "This content appears after the code block."
  }
})
```

### What does that look like?

```{r example, first=TRUE}
"This is the content of the code block."
```

## Let's try something more complex

```{r define_hook}
knitr::knit_hooks$set(deco = function(before, options) {
  if (before) {
    label <- ifelse(is.null(options$deco$label), "",
                    paste0("<b>", options$deco$label, "</b>"))
    colour <- ifelse(is.null(options$deco$colour), "#000000",
                     options$deco$colour)
    icon <- ifelse(is.null(options$deco$icon), "", 
                   paste0(fa(name = options$deco$icon, fill = colour), "  "))
    
    paste0("<span style=\"font-size:90%;color:", colour, "\">", icon, label, "</span>")
  }
})

```

```{r ex_label, deco=list(label="Labelled Cell")}
"This cell has a label"
```

```{r ex_icon, deco=list(label="Labelled Cell", icon="check")}
"This cell has a label and an icon"
```

```{r ex_colour, deco=list(label="Labelled Cell", icon="check", colour="#ff00ff")}
"This cell has a colourful label and icon"
```


### Example in R

```{r define_download_data_r, deco=list(label="R", icon="r-project", colour="#2165b6")}
download_data <- function(station) {
  r <- if (station == "catalogue") {
    GET("https://api.conr.ca/pcd/catalogue")
  } else {
    POST(paste0("https://api.conr.ca/pcd/get?station=", station))
  }
  unserialize(content(r))
}
```

```{r download_data_catalogue, deco=list(label="R", icon="r-project", colour="#2165b6")}
# An empty station_search() is the same as download_data("catalogue")
head(download_data("catalogue"))
```

```{r download_data_000401, deco=list(label="R", icon="r-project", colour="#2165b6")}
head(download_data("000401"))
```

### Example in Python

```{python define_download_data_python, deco=list(label="Python", icon="python", colour="#ffd648")}
def download_data(station):
    if station == "catalogue":
        r = requests.get("https://api.conr.ca/pcd/catalogue")
    else:
        r = requests.post("https://api.conr.ca/pcd/get?station={}".format(station))
    return pd.read_json(BytesIO(r.content))

```

```{python download_data_catalogue_py, deco=list(label="Python", icon="python", colour="#ffd648")}
print(download_data("catalogue"))
```

```{python download_data_000401_py, deco=list(label="Python", icon="python", colour="#ffd648")}
print(download_data("000401"))
```

### Example in Bash

_JSON processed through [jq](https://stedolan.github.io/jq/)_

```{bash download_data_catalogue_jq, deco=list(label="Shell", icon="terminal", colour="#4da825"), eval=params$bash}
curl -s -X GET "https://api.conr.ca/pcd/catalogue" | jq -r '.[0:10] | (map(keys) | add | unique) as $cols | map(. as $row | $cols | map($row[.])) as $rows | $cols, $rows[] | @csv'
```

```{bash download_data_000401_jq, deco=list(label="Shell", icon="terminal", colour="#4da825"), eval=params$bash}
curl -s -X POST "https://api.conr.ca/pcd/get?station=000401" | jq -r '.[396:406] | (map(keys) | add | unique) as $cols | map(. as $row | $cols | map($row[.])) as $rows | $cols, $rows[] | @csv'
```

## Exercise 5

Create a knitr hook that will add the following text before a code chunk:

`This code was written by NAME`

`NAME` should be a configurable option. 

The same hook should add the following output _after_ the code chunk:

`This code was executed at TIME`

`TIME` should be the output of `format(Sys.time(), "%H:%M:%S")`
