---
title: 'Markdown syntax?'
author: "Conor I Anderson"
date: "19/07/2020"
output: html_document
---

Markdown uses a series of special characters: 

```
\ ` * _ { } [ ] ( ) # + - . !
```

To use (most of) these characters in your text. You will have to escape them with a backslash. If you are writing in RStudio, the characters will turn blue when they are an escaped special character. 

**Input:**

```
\\ \` \* \_ \{ \} [ ] ( ) \# \+ \- \. \!
```

**Output:**

 \\ \` \* \_ \{ \} [ ] ( ) \# \+ \- \. \!


### Text formatting

Most things that you type will be plain text

End a line with two spaces to start a new paragraph.   

The asterix and the underscore control **bold** and *italic* text. 

- `*italics* and _italics_` becomes *italics* and _italics_   
- `**bold** and __bold__` becomes **bold** and __bold__   
- `***bold italic*** and ___bold italic___` becomes ***bold italic*** and ___bold italic___
- `superscript^2^` becomes superscript^2^
- `subscript~2~` becomes subscript~2~
  - These can be combined, of course:
    - `You _could_ drink x glasses of H~2~O, but **I** ***have to*** to drink x^2^ glasses after a run!` 
    - You _could_ drink _x_ glasses of H~2~O, but **I** ***have to*** drink _x_^2^ glasses after a run!
- One \~ will output subscript. Use two for `~~strikethrough~~` ~~strikethrough~~
  - `~~H~2~0~~` becomes ~~H~2~0~~

### Headings

This is header level 1
======================

```
This is header level 1
======================
```

This is header level 2
----------------------

```
This is header level 2
----------------------
```

# This is also header level 1

`# This is also header level 1`

## This is also header level 2

`## This is also header level 2`

### This is header level 3

`### This is header level 3`

#### This is header level 4

`#### This is header level 4`

##### This is header level 5

`##### This is header level 5`

###### This is header level 6

`###### This is header level 6`

####### There is no header level 7

`####### There is no header level 7`

No really, there is no header level 7. 

### Inserting Things

A link is composed of square brackets (link text) and round brackets (link location).

`[link](https://posit.co/)` will appear as [link](https://posit.co/)

Images use a similar syntax to links, preceded by an exclamation mark! 

`![](https://raw.githubusercontent.com/dcurtis/markdown-mark/master/svg/markdown-mark.svg)`
![](https://raw.githubusercontent.com/dcurtis/markdown-mark/master/svg/markdown-mark.svg)

You can insert equations using LaTeX math markup enclosed in dollar signs: 
- `$A = \pi*r^{2}$`
- $A = \pi*r^{2}$

You can insert bits of code or other (fixed-width text) using backticks (\`)
```
`A single backtick on each side appears inline`
```
`A single backtick on each side appears inline`

````
```
Three backticks appear in a paragraph block. This is useful for a large block
of multiple
lines
of
code
```
````

```
Three backticks appear in a paragraph block. This is useful for a large block
of multiple
lines
of
code
```

Some additional symbols:

  - endash: `--` --
  - emdash: `---` ---
  - ellipsis: `...` ...
  - horizontal rule (or slide break):  `***` 

***

You can also use any regular HTML markup for symbols. 

- `&#8594;` &#8594;
- `&#128077;` &#128077;
- Most special HTML characters can be written normally, e.g. & does not need to be `&amp;` 

### Quotes

`> block quote`

> block quote   

### Lists

You can create unordered lists by using a dash, an asterix, or a plus sign

```
- This is
- a list
```

- This is
- a list


```
* This is
* a list
```

* This is
* a list

```
+ This is
+ a list
```

+ This is
+ a list

Sub-items can be added using two spaces

```
- This is
- a list
  - This list has
  - Sub items
    - And it keeps
    - Going!
```

- This is
- a list
  - This list has
  - Sub items
    - And it keeps
    - Going!

Lists can be numbered. You can mix numbered and un-numbered items

```
1. ordered list 
2. item 2
- This item is not numbered
  + sub-item 1
  1. sub-item 2
  2. why is 2 numbered 1?
3. You probably shouldn't mix things like this as it can be ugly
```

1. ordered list 
2. item 2     
- This item is not numbered
  + sub-item 1
  1. sub-item 2
  2. why is 2 numbered 1?
3. You probably shouldn't mix things like this as it can be ugly
  
## Tables

Tables are easy to type, but it can be a little tedious. You may want to use a generator such as [this one](https://www.tablesgenerator.com/markdown_tables).

```
Table Header | Second Header 
------------ | ------------- 
Table Cell   | Cell 2        
Cell 3       | Cell 4
```

Table Header | Second Header 
------------ | ------------- 
Table Cell   | Cell 2        
Cell 3       | Cell 4

Use colons to specify table alignment


```
| Tables   |      Are      |  Cool  |
|----------|:-------------:|-------:|
| col 1 is | left-aligned  | tables |
| col 2 is | centered      | aren't |
| col 3 is | right-aligned | cool   |
```

| Tables   |      Are      |  Cool  |
|----------|:-------------:|-------:|
| col 1 is |  left-aligned | tables |
| col 2 is |    centered   | aren't |
| col 3 is | right-aligned | cool   |

## Yay

Now you know the basics of Markdown syntax. There are many cheat sheets to help you to remember if you forget: 

- [Markdown Cheet Sheet](https://markdownguide.offshoot.io/cheat-sheet/)
